use chrono::NaiveDateTime;

/// # Safety
///
/// No safety issue to mentioned here
#[no_mangle]
pub extern "C" fn print_date(ms: i64) -> bool {
    if let Some(dt) = NaiveDateTime::from_timestamp_millis(ms) {
        println!("{}", dt.format("%d/%m/%Y %H:%M:%S"));
    }
    true
}

/// # Safety
///
/// No safety issue to mentioned here
#[no_mangle]
pub extern "C" fn test_mode() {
    println!("Hello world in rust from C!");
}
