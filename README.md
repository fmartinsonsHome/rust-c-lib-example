# Rust C Lib Example

A little demonstration to compile a C-ABI compatible rust library
with its header, .so and a .pc file

For that you need to install cargo-c (the version depends on the version of cargo you have).
For example version 0.9.18 is compatible with cargo 1.67
```bash
cargo install cargo-c --version 0.9.18
```

Then you can compile the library
```bash
OUT_DIR=$(mktemp -d)/rust_c_lic_example_install
cargo cinstall \
      --prefix ${OUT_DIR} \
      --library-type cdylib
```

To compile the C wrapper that used the previous libray:
```bash
PKG_CONFIG_PATH=${OUT_DIR}/lib/pkgconfig meson _build
ninja -C _build
```

To run the binary
```bash
./_build/c-src/c_wrapper_client_rust
```
