#include <sys/time.h>
#include <stdio.h>
#include <rust_lib_example.h>

int main(int argc, char** argv)
{
    (void)argv;

    if (argc > 1) {
        test_mode();
    } else {
        struct timeval now;
        gettimeofday (&now, NULL);
        if (print_date(now.tv_sec * 1000 + (int)(now.tv_usec / 1000))) {
          fprintf(stderr,"rust func ok\n");
        } else {
          fprintf(stderr,"rust func error\n");
        }
    }
    return 0;
}
